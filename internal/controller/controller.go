package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"questions-api/internal/storage/models"
	"questions-api/pkg/prometheus"
)

type Logger interface {
	Error(...interface{})
	Panic(...interface{})
}

type Servicer interface {
	GetQuestions(context.Context, string) (*models.QuestionList, error)
	AddQuestion(ctx context.Context, question *models.Question) (*models.Question, error)
}

type Controller struct {
	lg  Logger
	svc Servicer
}

func Init(svc Servicer, lg Logger) *Controller {
	return &Controller{
		lg:  lg,
		svc: svc,
	}
}

func (cont *Controller) GetQuestions(w http.ResponseWriter, r *http.Request) {
	lang := r.URL.Query().Get("lang")
	if lang == "" {
		err := fmt.Errorf("lang query parameter not provided")
		prometheus.CountFailedAction("ReadQueryParam", err.Error())
		cont.writeError(w, err, http.StatusBadRequest)
		return
	}

	result, err := cont.svc.GetQuestions(r.Context(), lang)
	if err != nil {
		prometheus.CountFailedAction("GetQuestions", err.Error())
		cont.writeError(w, err, http.StatusInternalServerError)
		return
	}

	resultBytes, err := json.Marshal(result)
	if err != nil {
		prometheus.CountFailedAction("MarshalRespBody", err.Error())
		cont.writeError(w, err, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write(resultBytes); err != nil {
		prometheus.CountFailedAction("WriteResponse", err.Error())
		cont.lg.Error(err)
		return
	}
	prometheus.CountOKAction("GetQuestions")
}

func (cont *Controller) AddQuestion(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		prometheus.CountFailedAction("ReadRqBody", err.Error())
		cont.writeError(w, err, http.StatusBadRequest)
		return
	}

	var question models.Question
	if err = json.Unmarshal(body, &question); err != nil {
		prometheus.CountFailedAction("UnmarshalRqBody", err.Error())
		cont.writeError(w, err, http.StatusBadRequest)
		return
	}

	result, err := cont.svc.AddQuestion(r.Context(), &question)
	if err != nil {
		prometheus.CountFailedAction("AddQuestion", err.Error())
		cont.writeError(w, err, http.StatusInternalServerError)
		return
	}

	resultBytes, err := json.Marshal(result)
	if err != nil {
		prometheus.CountFailedAction("MarshalRespBody", err.Error())
		cont.writeError(w, err, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write(resultBytes); err != nil {
		prometheus.CountFailedAction("WriteResponse", err.Error())
		cont.lg.Error(err)
		return
	}
	prometheus.CountOKAction("AddQuestion")
}

func (cont *Controller) writeError(w http.ResponseWriter, err error, header int) {
	cont.lg.Error(err)
	w.WriteHeader(header)

	errMap := map[string]string{"error": err.Error()}
	if err = json.NewEncoder(w).Encode(errMap); err != nil {
		cont.lg.Error(err)
	}
}
