package storage

import (
	"context"
	"errors"
	"fmt"
	"questions-api/internal/storage/csv"
	"questions-api/internal/storage/json"
	"questions-api/internal/storage/models"
	"sync"
)

type Logger interface {
	Panic(...interface{})
}

type Processor interface {
	ReadQuestions() (*models.QuestionList, error)
	SaveQuestions(context.Context, []models.Question) error
}

type Storager struct {
	cache      *models.QuestionList
	cacheMutex sync.Mutex
	proc       Processor
}

func Init(lg Logger, storageType string) *Storager {
	var proc Processor
	switch storageType {
	case "json":
		proc = json.New()
	case "csv":
		proc = csv.New()
	default:
		lg.Panic(fmt.Errorf("Unknown storage type %s", storageType))
	}

	questions, err := proc.ReadQuestions()
	if err != nil {
		lg.Panic(err)
	}

	return &Storager{
		cache:      questions,
		cacheMutex: sync.Mutex{},
		proc:       proc,
	}
}

func (st *Storager) GetQuestions(context.Context) (*models.QuestionList, error) {
	return st.cache, nil
}

func (st *Storager) AddQuestion(ctx context.Context, question *models.Question) (*models.Question, error) {
	if question == nil {
		return nil, errors.New("Can't add an empty question")
	}

	st.cacheMutex.Lock()
	defer st.cacheMutex.Unlock()

	st.cache.Data = append(st.cache.Data, *question)
	err := st.proc.SaveQuestions(ctx, st.cache.Data)
	return question, err
}
