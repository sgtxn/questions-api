package json

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"os"
	"questions-api/internal/storage/models"
)

type Processor struct{}

func New() *Processor {
	return &Processor{}
}

func (p Processor) ReadQuestions() (*models.QuestionList, error) {
	bytes, err := ioutil.ReadFile("data/questions.json")
	if err != nil {
		return nil, err
	}

	var questions []models.Question

	if err := json.Unmarshal(bytes, &questions); err != nil {
		return nil, err
	}

	return &models.QuestionList{Data: questions}, nil
}

func (p Processor) SaveQuestions(ctx context.Context, allQuestions []models.Question) error {
	file, err := os.OpenFile("data/questions.json", os.O_WRONLY, os.ModePerm)
	if err != nil {
		return err
	}

	enc := json.NewEncoder(file)
	enc.SetIndent("", "  ")

	return enc.Encode(allQuestions)
}
