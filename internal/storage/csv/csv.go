package csv

import (
	"context"
	"encoding/csv"
	"fmt"
	"os"
	"questions-api/internal/storage/models"
)

type Processor struct{}

func New() Processor {
	return Processor{}
}

func (p Processor) ReadQuestions() (*models.QuestionList, error) {
	file, err := os.Open("data/questions.csv")
	if err != nil {
		return nil, err
	}

	var questions []models.Question
	r := csv.NewReader(file)
	r.LazyQuotes = true
	r.TrimLeadingSpace = true

	records, err := r.ReadAll()
	if err != nil {
		return nil, err
	}

	for i, record := range records {
		if i == 0 { //We don't need the header row for now
			continue
		}

		if len(record) != 5 {
			return nil, fmt.Errorf("Got a record with an unexpected number of fields: %d", len(record))
		}

		//TODO: check out external libs such as gocsv, cause this doesn't seem to be the best approach
		question := models.Question{
			Text:      record[0],
			CreatedAt: record[1],
			Choices: []models.Choice{
				{Text: record[2]},
				{Text: record[3]},
				{Text: record[4]},
			},
		}

		questions = append(questions, question)
	}

	return &models.QuestionList{Data: questions}, nil
}

func (p Processor) SaveQuestions(ctx context.Context, allQuestions []models.Question) error {
	file, err := os.OpenFile("data/questions.csv", os.O_WRONLY, os.ModePerm)
	if err != nil {
		return err
	}

	w := csv.NewWriter(file)
	header := []string{"Question text", "Created At", "Choice 1", "Choice", "Choice 3"}
	if err := w.Write(header); err != nil {
		return err
	}

	for _, question := range allQuestions {
		records := []string{
			question.Text,
			question.CreatedAt,
			question.Choices[0].Text,
			question.Choices[1].Text,
			question.Choices[2].Text,
		}

		if err := w.Write(records); err != nil {
			return err
		}
	}

	w.Flush()
	return w.Error()
}
