package models

type QuestionList struct {
	Data []Question `json:"data"`
}

type Question struct {
	Text      string   `json:"text"`
	CreatedAt string   `json:"createdAt"` // TODO: change this to time.Time, which might be needed in the future
	Choices   []Choice `json:"choices"`
}

type Choice struct {
	Text string `json:"text"`
}
