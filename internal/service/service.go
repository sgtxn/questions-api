package service

import (
	"context"
	"questions-api/internal/storage/models"
)

type Storager interface {
	GetQuestions(context.Context) (*models.QuestionList, error)
	AddQuestion(context.Context, *models.Question) (*models.Question, error)
}

type Translator interface {
	Translate(context.Context, *models.QuestionList, string) (*models.QuestionList, error)
}

type Service struct {
	storage    Storager
	translator Translator
}

func Init(st Storager, tl Translator) *Service {
	return &Service{
		storage:    st,
		translator: tl,
	}
}

func (svc *Service) GetQuestions(ctx context.Context, lang string) (*models.QuestionList, error) {
	if err := validateLanguage(lang); err != nil {
		return nil, err
	}

	questions, err := svc.storage.GetQuestions(ctx)
	if err != nil {
		return nil, err
	}

	// Assuming our questions are stored in English
	if lang != "en" {
		questions, err = svc.translator.Translate(ctx, questions, lang)
		if err != nil {
			return nil, err
		}
	}

	return questions, nil
}

func (svc *Service) AddQuestion(ctx context.Context, question *models.Question) (*models.Question, error) {
	if err := validateQuestion(question); err != nil {
		return nil, err
	}

	return svc.storage.AddQuestion(ctx, question)
}
