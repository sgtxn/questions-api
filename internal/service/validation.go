package service

import (
	"errors"
	"fmt"
	"questions-api/internal/storage/models"

	iso6391 "github.com/emvi/iso-639-1"
)

func validateLanguage(lang string) error {
	if !iso6391.ValidCode(lang) {
		return fmt.Errorf("Got an invalid language code: %s", lang)
	}
	return nil
}

func validateQuestion(question *models.Question) error {
	if question == nil {
		return errors.New("Got an empty question")
	}

	if question.Text == "" {
		return errors.New("Got a question with no text")
	}

	if question.CreatedAt == "" {
		return errors.New("Got a question with no creation time")
	}

	if len(question.Choices) != 3 {
		return fmt.Errorf("Got a questions with a number of choices I didn't expect: %d", len(question.Choices))
	}

	return nil
}
