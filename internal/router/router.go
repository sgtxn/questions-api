package router

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	apiV1Prefix = "/api/v1"
)

type Controller interface {
	GetQuestions(http.ResponseWriter, *http.Request)
	AddQuestion(http.ResponseWriter, *http.Request)
}

func Init(cont Controller) *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.StripSlashes)

	router.Route(apiV1Prefix, func(r chi.Router) {
		r.Get("/questions", cont.GetQuestions)
		r.Post("/questions", cont.AddQuestion)
	})

	router.Method(http.MethodGet, "/metrics", promhttp.Handler())

	return router
}
