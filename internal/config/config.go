package config

import (
	"github.com/BurntSushi/toml"
)

type Logger interface {
	Panic(...interface{})
}

type Config struct {
	Application Application
	Storage     Storage
}

type Application struct {
	Name string
	Port uint16
}

type Storage struct {
	Type string
}

func Init(lg Logger) *Config {
	var cfg Config
	_, err := toml.DecodeFile("configs/config.toml", &cfg)
	if err != nil {
		lg.Panic(err)
	}

	return &cfg
}
