package translator

import (
	"context"
	"fmt"
	"questions-api/internal/storage/models"
	"questions-api/pkg/prometheus"
	"time"

	"cloud.google.com/go/translate"
	"golang.org/x/text/language"
)

type GoogleTranslator struct{}

func Init() *GoogleTranslator {
	return &GoogleTranslator{}
}

func (tl *GoogleTranslator) Translate(ctx context.Context, questions *models.QuestionList, targetLanguage string) (*models.QuestionList, error) {
	lang, err := language.Parse(targetLanguage)
	if err != nil {
		return nil, fmt.Errorf("language.Parse: %v", err)
	}

	client, err := translate.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	defer client.Close()

	//TODO: explore options to reduce the number of calls to gtranslate api
	var results []models.Question
	for i := range questions.Data {
		inputs := []string{
			questions.Data[i].Text,
		}
		for _, choice := range questions.Data[i].Choices {
			inputs = append(inputs, choice.Text)
		}

		expectedLength := len(questions.Data[i].Choices) + 1

		rqCtx, cancel := context.WithTimeout(ctx, time.Second*5)
		defer cancel()

		resps, err := client.Translate(rqCtx, inputs, lang, nil)
		if err != nil {
			return nil, err
		}
		if len(resps) != expectedLength {
			return nil, fmt.Errorf("GTranslate returned wrong response to text: %v", inputs)
		}

		var choices []models.Choice
		for _, resp := range resps[1:] {
			choices = append(choices, models.Choice{Text: resp.Text})
		}

		result := models.Question{
			Text:      resps[0].Text,
			CreatedAt: questions.Data[i].CreatedAt,
			Choices:   choices,
		}

		results = append(results, result)
		prometheus.CountOKAction("Translate")
	}

	return &models.QuestionList{Data: results}, nil
}
