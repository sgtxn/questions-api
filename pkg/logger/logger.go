package logger

import "go.uber.org/zap"

func Init() *zap.SugaredLogger {
	lg, err := zap.NewProduction(zap.AddCaller())
	if err != nil {
		panic(err)
	}
	return lg.Sugar()
}
