package prometheus

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	timerAction = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name:       "timerAction",
			Help:       "Timer running action",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
		[]string{"method"})
	actionCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "actionCounter",
			Help: "Counter actions",
		},
		[]string{"location", "description", "state"})
)

func Init() {
	prometheus.MustRegister(timerAction)
	prometheus.MustRegister(actionCounter)
}

func WriteActionTime(start time.Time, source string) {
	timerAction.WithLabelValues(source).Observe(float64(time.Since(start).Seconds()))
}

func CountOKAction(source string) {
	actionCounter.WithLabelValues(source, "OK", "success").Inc()
}

func CountFailedAction(source string, reason string) {
	actionCounter.WithLabelValues(source, reason, "failure").Inc()
}
