# questions-api

A simple service that can store and return questions with their respective possible answers.  
Can optionally translate questions and answers into other languages. 

## Building and launching

```
go run cmd/questions-api/main.go
```

In order for Google Translate function to work you need a service account at google cloud.  
```
export GOOGLE_APPLICATION_CREDENTIALS=/home/username/path/service_account_key.json
```
Initial question data goes into **data/questions.json** or **data/questions.csv**.  
Storage type can be switched in **configs/config.toml -> Storage -> Type**. Possible values are **json** and **csv**.

## API

### <a href="api/swagger.yaml">Swagger docs.</a>

## Metrics server
Accessible at **http://localhost:8080/metrics**
