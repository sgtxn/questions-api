module questions-api

go 1.15

require (
	cloud.google.com/go v0.76.0
	github.com/BurntSushi/toml v0.3.1
	github.com/emvi/iso-639-1 v1.0.1
	github.com/go-chi/chi v1.5.1
	github.com/prometheus/client_golang v1.9.0
	go.uber.org/zap v1.16.0
	golang.org/x/text v0.3.5
)
