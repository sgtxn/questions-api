package main

import (
	"log"
	"net/http"
	"questions-api/internal/config"
	"questions-api/internal/controller"
	"questions-api/internal/router"
	"questions-api/internal/service"
	"questions-api/internal/storage"
	"questions-api/internal/translator"
	"questions-api/pkg/logger"
	"questions-api/pkg/prometheus"
	"strconv"
	"time"
)

const httpTimeout = 10 * time.Second

func main() {
	lg := logger.Init()
	cfg := config.Init(lg)
	prometheus.Init()

	tl := translator.Init()
	storage := storage.Init(lg, cfg.Storage.Type)
	svc := service.Init(storage, tl)
	cont := controller.Init(svc, lg)
	router := router.Init(cont)

	addr := ":" + strconv.FormatUint(uint64(cfg.Application.Port), 10)
	server := http.Server{
		Addr:         addr,
		Handler:      router,
		ReadTimeout:  httpTimeout,
		WriteTimeout: httpTimeout,
	}

	lg.Infof("Started listening on address %s", addr)
	log.Fatal(server.ListenAndServe())
}
